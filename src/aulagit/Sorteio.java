package aulagit;


import java.util.Random;

public class Sorteio
{
    // instance variables - replace the example below with your own
    private String[] alunos = {"ISABELLI LAGO LEON PATUSSI",
"FABIO RODRIGUES GONCALVES FILHO",
"GABRIEL VIÉGAS DA SILVA",
"ALEX JÚNIOR PADILHA PEREIRA",
"ARTHUR OLIVEIRA DE ROSSO",
"AUGUSTO FALCãO FLACH",
"BRENDA ANGHINONI BARBOSA",
"EDUARDA COSTA DE BRITO",
"ENRICO DOS SANTOS BARRAGON",
"ERICK DOS SANTOS BARRAGON",
"GABRIELA COUTO GLEN",
"JÚLIA CARDOSO HERNANDES",
"LARISSA SILVA DA ROSA",
"LAURI DUTRA JÚNIOR",
"LUCAS SBARDELOTTO ALI",
"LUCAS SOMBRA ALMEIDA",
"PEDRO HENRIQUE ROCHA BLAUTH",
"PIETRO BENATI CARRARA",
"RAFAELLA SANTANA BUENO",
"DANIEL GARBIN DOEGE",
"FABIO DE FREITAS SANTANA",
"GUILHERME BRAGAGNOLLO TEIXEIRA",
"JESSÉ LUCAS ELIAS FERREIRA"
};

    public Sorteio(){
        embaralhar();
        embaralhar();
        
    }
    
    public void imprimirDuplas(){
        for (int i=0; i < alunos.length-2; i++) {

            System.out.println(" Dupla (" +alunos[i] +", "+ alunos[++i] +")");
		}
		System.out.println(alunos[alunos.length-1]);
    }
    
    
    private void embaralhar() {
		
		Random random = new Random();
		
		for (int i=0; i < (alunos.length - 1); i++) {

			//sorteia um índice
			int j = random.nextInt(alunos.length); 
			
			//troca o conteúdo dos índices i e j do vetor
			String temp = alunos[i];
			alunos[i] = alunos[j];
			alunos[j] = temp;
		}
		
	}

 
}
